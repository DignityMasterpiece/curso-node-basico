import express from 'express'
import morgan from 'morgan'
import cors from 'cors'
import compression from 'compression'
import helmet from 'helmet'
import bodyParser from 'body-parser'
// const router = express.Router();
import indexRouter from './routes/index';
import movieRouter from './routes/movie';
import './database/mongoConnection'
const app = express();
const port = 4000
app.use(helmet()) //agrega seguridad al api
app.use(morgan('dev')) //loguea las peticiones {http  verb} {endpoint} {status code} - {latency}
app.use(compression()) //adelgaza el cuerpo de la petición http
app.use(bodyParser.json())
app.use(express.urlencoded({ extended: true }))
app.use('/api/v1/', [], indexRouter)
app.use('/api/v1/movie', [], movieRouter)
app.listen(port, () => {
  console.log(`server start on - ${port} environment - ${process.env.NODE_ENV}`)
})
