import mongoose from 'mongoose';
mongoose
  .connect(
    'mongodb://localhost:27017/movie-theater',
    { useNewUrlParser: true, useUnifiedTopology: true })
  .then(db => console.log(`movie-theater database connected`))
  .catch(e => console.error(e))
