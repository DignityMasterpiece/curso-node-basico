import express from 'express';
import movieController from '../controllers/movieController'
const router = express.Router();
import example from '../middlewares/example'
router.get('/', [], movieController.getById)

router.post('/', [example.midd_1, example.midd_2], movieController.create)
module.exports = router;
