import Movie from '../models/movie'
exports.create = (req, res) => {
  try {
    let { name, description, filmGenre } = req.body;
    console.log(name, description, filmGenre);
    let movie = new Movie({
      name,
      description,
      film_genre: filmGenre
    })
    movie.save((err, movie) => {
      try {
        if (err) {
          throw (err)
        }
        res.send({
          success: true,
          result: {
            movie
          }
        })
      } catch (error) {
        console.log(2)
        res.send(JSON.stringify(error))
      }
    })
  } catch (error) {
    res.send(JSON.stringify(error))
  }
}

exports.createAsync = async (req, res) => {
  try {
    let { name, description, filmGenre } = req.body;
    console.log(name, description, filmGenre);
    let movie = new Movie({
      name,
      description,
      film_genre: filmGenre
    })
    let newMovie = await movie.save();

    res.send({
      success: true,
      result: {
        movie: newMovie
      }
    })
  } catch (error) {
    console.log(1)
    res.send(JSON.stringify(error))
  }
}
exports.getById = async (req, res) => {
  try {
    let { id } = req.query;
    let movie = await Movie.findById(id)
    res.send({
      success: true,
      result: {
        movie
      }
    })
  } catch (error) {
    res.send({
      success: false,
      error: JSON.stringify(error)
    })
  }

}
