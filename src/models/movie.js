import mongoose from 'mongoose';
const movieSchema = new mongoose.Schema({
  title: {
    type: String,
  },
  description: {
    type: String
  },
  film_genre: {
    type: String,
    enum: ['romantic', 'action', 'fiction']
  }
}, { collection: 'movie' })
module.exports = mongoose.model('Movie', movieSchema);
